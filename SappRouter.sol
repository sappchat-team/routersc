pragma solidity =0.6.6;
import "./interfaces/IPancakeRouter.sol";
import "./interfaces/IERC20.sol";
import "./libraries/Ownable.sol";


contract SappRouter is Ownable{
    address private router;
    address private APP_TOKEN;
    address private wallet;
    uint private APP_FEE = 100000000000000000000; // 1 ETH
    constructor(address _router, address _token, address _wallet, uint _appFee) public {
        router = _router;
        APP_TOKEN = _token;
        wallet = _wallet;
        APP_FEE = _appFee;
    }

    function setRouter (address _router) public onlyOwner returns(bool){
        _setRouter(_router);
        return true;
    }

    function setToken (address _token) public onlyOwner returns(bool){
        _setToken(_token);
        return true;
    }

    function setWallet(address _wallet) public onlyOwner returns(bool){
        _setWallet(_wallet);
        return true;
    }

    function setAppFee(uint _appFee) public onlyOwner returns(bool){
        _setAppFee(_appFee);
        return true;
    }

    modifier payAppFee() {
        // Setup app fee
        // Approve address(this) with APP_FEE at APP_TOKEN contract first
        // value is APP_FEE
        // Or APP_FEE + amountIn(or more) for case APP_TOKEN === path[0];
        require(IERC20(APP_TOKEN).transferFrom(msg.sender, wallet, APP_FEE), "PAY_FEE_FIRST");
        _;
    }

    // **** SWAP ****
    function swapETHForExactTokens(uint amountOut, address[] calldata path, address to, uint deadline)
    external
    payable
    payAppFee
    returns (uint[] memory amounts) {

        (amounts) = IPancakeRouter02(router).swapETHForExactTokens{value: msg.value}(amountOut, path, to, deadline);
    }

    function swapExactETHForTokens(uint amountOutMin, address[] calldata path, address to, uint deadline)
    external
    payable
    payAppFee
    returns (uint[] memory amounts) {

        (amounts) = IPancakeRouter02(router).swapExactETHForTokens{value: msg.value}(
            amountOutMin,
            path,
            to,
            deadline
        );
    }

    function swapExactETHForTokensSupportingFeeOnTransferTokens(
    uint amountOutMin,
    address[] calldata path,
    address to,
    uint deadline
    ) external payable payAppFee {

        IPancakeRouter02(router).swapExactETHForTokensSupportingFeeOnTransferTokens{value: msg.value}(
            amountOutMin,
            path,
            to,
            deadline
        );
    }

    function swapExactTokensForETH(uint amountIn, uint amountOutMin, address[] calldata path, address to, uint deadline)
    external
    payAppFee
    returns (uint[] memory amounts) {
        _handleToken(path[0], amountIn);

        (amounts) = IPancakeRouter02(router).swapExactTokensForETH(amountIn, amountOutMin, path, to, deadline);
    }

    function swapExactTokensForETHSupportingFeeOnTransferTokens(
    uint amountIn,
    uint amountOutMin,
    address[] calldata path,
    address to,
    uint deadline
    ) external payAppFee {
        _handleToken(path[0], amountIn);

        IPancakeRouter02(router).swapExactTokensForETHSupportingFeeOnTransferTokens(amountIn, amountOutMin, path, to, deadline);
    }

    function swapExactTokensForTokens(
    uint amountIn,
    uint amountOutMin,
    address[] calldata path,
    address to,
    uint deadline
    ) external payAppFee returns (uint[] memory amounts) {
        _handleToken(path[0], amountIn);

        (amounts) = IPancakeRouter02(router).swapExactTokensForTokens(amountIn, amountOutMin, path, to, deadline);
    }

    function swapExactTokensForTokensSupportingFeeOnTransferTokens(
    uint amountIn,
    uint amountOutMin,
    address[] calldata path,
    address to,
    uint deadline
    ) external payAppFee {
        _handleToken(path[0], amountIn);

        IPancakeRouter02(router).swapExactTokensForTokensSupportingFeeOnTransferTokens(amountIn, amountOutMin, path, to, deadline);
    }

    function swapTokensForExactETH(uint amountOut, uint amountInMax, address[] calldata path, address to, uint deadline)
    external
    payAppFee
    returns (uint[] memory amounts) {
        _handleToken(path[0], amountInMax);

        (amounts) = IPancakeRouter02(router).swapTokensForExactETH(amountOut, amountInMax, path, to, deadline);
    }

    function swapTokensForExactTokens(
    uint amountOut,
    uint amountInMax,
    address[] calldata path,
    address to,
    uint deadline
    ) external payAppFee returns (uint[] memory amounts) {
        
        _handleToken(path[0], amountInMax);

        (amounts) = IPancakeRouter02(router).swapTokensForExactTokens(amountOut, amountInMax, path, to, deadline);
    }

    function _handleToken(address _token, uint _amount) internal {
        // Need to approve at path[0] token contract
        // value is _amount or more
        require(IERC20(_token).transferFrom(msg.sender, address(this), _amount), "Transfer Failed");
        require(IERC20(_token).approve(address(router), _amount), "Approve Failed");
    }

    function _setRouter(address _router) internal {
        router = _router;
    }
    function _setToken(address _token) internal {
        APP_TOKEN = _token;
    }
    function _setWallet(address _wallet) internal {
        wallet = _wallet;
    }
    function _setAppFee(uint _appFee) internal {
        APP_FEE = _appFee;
    }

    function _getRouter() public view onlyOwner returns (address) {
        return router;
    }
    function _getToken() public view onlyOwner returns (address) {
        return APP_TOKEN;
    }
    function _getWallet() public view onlyOwner returns (address) {
        return wallet;
    }
    function _getAppFee() public view onlyOwner returns (uint) {
        return APP_FEE;
    }

}
